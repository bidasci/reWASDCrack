# NOTICE 18.12.2023
- Crack for 6.6 ver ready with youtube guide for those who really wants it, check releases. Version 7.0 crack will be later.
- Virtual controllers may not work for everyone in 6.6ver., wait 7.0ver
- May this holiday season brighten you up with happiness, joy and good cheer!

# CURRENT STATUS
- 7.0ver. ARM - FULL WORK! x86/x64 - WAITING CRACK
- 6.6ver x86/x64 - WORK 

# GUIDE FOR LITTLE ONES ONLY FOR 6.6ver. x64/x86 CRACK
- FIRST VIDEO GUIDE https://youtu.be/JovXbTjLo7w?si=7h8hVmr_YpCuj4OK
- SECOND VIDEO GUIDE(FOR NOOBS) https://youtu.be/tFF5TPHk_OQ?si=2setqKzMU0tXeCPd

# TEXT GUIDE 
1. Extract the files from the archive to the folder with reWASD
2. Change 4 paths in the file "1337_NI66A_PUSSYSLAYER_BREAKPOINT.xml". 
"C:\Program Files\reWASD\reWASDEngine.exe" to your path
3. Launch dnSpy.exe and drop reWASDEngine.exe into dnSpy
4. Add breapoint window in dnSpy ->Debug->Windows->Breakpoints
5. Import "1337_NI66A_PUSSYSLAYER_BREAKPOINT.xml" with special button in breapoint window
6. You should see 4 breakpoints, if there are less than 4 then you have done something wrong.
7. Launch reWASDEngine.exe(not reWASD.exe) only from dnSpy!!!!
8. Close reWASD only from tray windows and then close dnSpy

# Applying config and nothing happens? 6.6 ver.
FIRSTLY, CHECK CLOSED ISSUES AND TWO VIDEO GUIDES, BEFORE CREATING NEW, TAKE CARE OF YOUR AND MY TIME❤️

Most likely, you have not imported breakpoints
- There may be two reasons why they are not imported:
1. You need to select the current assembly in dnSpy, that is, click on reWASDEngine in Assembly editor, I did it in the video at 2:15.
2. Change the paths to the file reWASDEngine.exe in the file with breakpoints (1337_pus...xml), there are only 4 lines where you need to set the correct path to the file, in the video it is 1:46 minute (also in the video there is only 1 path because today the fix came out and now there are 4)

# Startup automation for dnSpy
Use autohotkey, make a simple file including lines below, and move it to startup folder, then it can be automaticly run when You start PC and placed in tray menu:
- Run, D:\reWASD1\dnSpy-net-win32\dnSpy.exe
- Sleep 3000
- Send {F5}
- Sleep 2000
- Send {Enter}
- Sleep 1000
- Winhide dnSpy

# Special Thanks
HUGE THANKS TO [Eddy](https://github.com/RedDot-3ND7355) ([reWASD](https://github.com/RedDot-3ND7355/reWASD)) ONLY THANKS TO HIM YOU CAN USE X64/86 VERSION FOR FREE, SO BEFORE DOWNLOADING GO TO HIS GITHUB PROFILE AND PUT STARS ON EACH OF HIS PROJECTS!!!!

# reWASD Source Code & Patched Lib's in Releases

![Screenshot 29-11-2023 at 22-15-35](https://github.com/EugeneSunrise/reWASD/assets/56397706/1d3e6290-73b2-4d19-a826-17667841aaed)


# USING. ITS ONLY FOR 7.0 VER. ARM(x86 dont work right now)
reWASD has are several versions, the version for Windows x86/x64 and Windows ARM Architecture.

Step by step for the littlest ones:
- First of all, delete your current version of reWasd program
- Go to [Releases](https://gitea.slowb.ro/bidasci/reWASDCrack/releases) and pick your version
- Do you see the Assets tab? Fine!
- Download installer in Assets for cracked version - reWASD700-8378(ARM).exe or reWASD700-8447.exe
- Install!
- Download reWASD.dll and reWASDCommon.dll(or reWASDEngine.dll for ARM) in Assets
- Disable reWASD (if enabled)
- Move reWASD.dll and reWASDCommon.dll( or reWASDEngine.dll) WITH REPLACEMENT in main reWasd folder
- Start reWASD, all features are available!


## Support the project financially

TRX(Tron): TGztJ8FSwKNSrigESddGq6euHa9UgtkujG

BTC(Bitcoin): bc1q9ym4ac67x9c2slg64lq3u8wczdadj4tep7y2fw

ETH(Ethereum): 0xb9Fa2B5661238eC9D825bdb0379Db5b035179282

**Thanks for the support!**

